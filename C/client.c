#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>   //port série
#include <unistd.h>
#include <curl/curl.h>  //utilisation de libCurl uniquement coté client
#include <string.h>
#include "cJSON.h"

#define PORT "/dev/ttyACM0"

int fd_usb;
char buffer[2048];    //donnees reçu par arduino
char buffer2[2048];

//-----Après découpage-------
char monTimestamp[1024] ;

int timestamp ;
char meteo[1024];
char prevision[1024];
ssize_t n ;

int main() {
    struct curl_slist *headers=NULL;
    headers = curl_slist_append(headers, "Content-Type: application/json");

    //----Réception des données envoyés par arduino
    fd_usb = open(PORT, O_RDONLY );   //ouverture du port série
    if(fd_usb < 0){
        perror(PORT);
        exit(-1);
    }
    printf("Ouverture du port Série réussie\n");

    memset(&buffer, '\0', 2048);
    memset(&buffer2, '\0', 2048);

    while(1) {  //pas le choix pour reboucler
        cJSON * objet ;
        cJSON *root = cJSON_CreateObject();    //stocke les data pour les envoyer à Heroku
        cJSON *weather = cJSON_CreateArray();
        cJSON *forecast_day = cJSON_CreateObject();
        cJSON *forecast_tomorrow = cJSON_CreateObject();

        CURL *curl ;    //handle
        CURLcode curlCR;

        int i = 0 ;
        int compteurCopie = 0 ;

        int compteurBuffer = 0 ;
        int compteurTimestamp = 0 ;
        int compteurMeteo = 0 ;
        int compteurPrevision = 0 ;

        memset(&monTimestamp, '\0', 1024);
        memset(&meteo, '\0', 1024);
        memset(&prevision, '\0', 1024);

        //Après la seconde lecture de notre port, on se retrouve au milieu d'une trame, ce qui enclenche l'erreur. Le mieux est de faire un read débile le temps de tomber sur la bonne trame
        n = read(fd_usb, &buffer, 100);
        if(n < 0){
            perror("Read error");
            exit(0);
        }

        while(buffer[i] != '[' && i < sizeof(buffer)){   //ça couille ici (debug)
            i++ ;
        }
        //j arrive à recuperer la première trame et à la placer dans mon second buffer MAIS le passage a la seconde bloque
        while(buffer[i+compteurCopie] != ']' && compteurCopie+i < sizeof(buffer)){
            buffer2[compteurCopie] = buffer[i+compteurCopie];
            compteurCopie++;
        }
        buffer2[compteurCopie] = buffer[i+compteurCopie];
        compteurBuffer++;

        printf("%s\n", buffer2); //format de la trame reçu : [0_SUNNY_CLOUDY]
//----Découpage des données ------/
        while (buffer2[compteurBuffer] != '_' && compteurTimestamp < sizeof(buffer2)) {
            monTimestamp[compteurTimestamp] = buffer2[compteurBuffer];
            compteurTimestamp++;
            compteurBuffer++;
        }
        compteurBuffer++;
        printf("Mon timestamp : %s\n", monTimestamp);
        while (buffer2[compteurBuffer] != '_' && compteurMeteo < sizeof(buffer2)) {
            meteo[compteurMeteo] = buffer2[compteurBuffer];
            compteurMeteo++;
            compteurBuffer++;
        }
        printf("Ma météo : %s\n", meteo);
        compteurBuffer++;
        while (buffer2[compteurBuffer] != ']' && compteurPrevision < sizeof(buffer2)) {
            prevision[compteurPrevision] = buffer2[compteurBuffer];
            compteurPrevision++;
            compteurBuffer++;
        }

        printf("Ma prévision : %s\n", prevision);
        timestamp = atoi(monTimestamp);

        //-----"conversion" des données en JSON-----/
        cJSON_AddNumberToObject(root,"timestamp",timestamp);
        cJSON_AddItemToObject(root,"weather",weather);

        cJSON_AddItemToArray(weather,forecast_day);
        cJSON_AddNumberToObject(forecast_day,"dfn",0);
        cJSON_AddStringToObject(forecast_day,"weather",meteo);

        cJSON_AddItemToArray(weather,forecast_tomorrow);
        cJSON_AddNumberToObject(forecast_tomorrow,"dfn",1);
        cJSON_AddStringToObject(forecast_tomorrow,"weather",prevision);

        char * out = cJSON_Print(root);
        printf("%s",out);

        //---- Partie Client envoyant des données au serveur Heroku -----/
        curl = curl_easy_init() ;
        if (!curl) {
            perror("Curl départ fail");
            exit(0);
        }

        // Spécification des options pour handler curl
        curl_easy_setopt(curl, CURLOPT_URL, "http://obscure-tundra-53830.herokuapp.com/metrology");
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, strlen(out)); //strlen : ne pas oublier que char * = pointeur vers une adresse je veux la taille de ce qu'elle pointe pas la taille de celle ci !
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, out);
        curl_easy_setopt(curl, CURLOPT_POST, 1L);

        curlCR = curl_easy_perform(curl); //se connecte au site et reçoit des données spécifiques

        if (curlCR != CURLE_OK) {
            perror("Echec de l'envoi");
            exit(0);
        }

        curl_easy_cleanup(curl);
        cJSON_Delete(root);
        free(out);
    }
}